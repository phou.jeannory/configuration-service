FROM java:8
ARG JAR_FILE=target/configuration-service.jar
ARG JAR_LIB_FILE=target/lib/

# cd /usr/local/runme
WORKDIR /usr/local/runme

# cp target/configuration-service.jar /usr/local/runme/configuration-service.jar
COPY ${JAR_FILE} configuration-service.jar

# cp -rf target/lib/  /usr/local/runme/lib
ADD ${JAR_LIB_FILE} lib/

# java -jar /usr/local/runme/configuration-service.jar
ENTRYPOINT ["java","-jar","configuration-service.jar"]